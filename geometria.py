# Autora = Martha Cango.
# Email = martha.cango@unl.edu.ec

import math
import pygame, sys
from pygame.locals import *

pygame.init()
ventana = pygame.display.set_mode((600, 600))
pygame.display.set_caption("Creando un rectángulo")
color = (0, 64, 64)
verde = (0, 255, 0)
rojo = (255, 0, 0)
azul_marino = (0, 64, 200)
blanco = (255, 255, 255)

ventana.fill(color)
pygame.draw.rect(ventana, (150, 100, 255), (300, 300, 200, 90))
pygame.draw.line(ventana, verde, (560, 300), (30, 300), 2)
pygame.draw.line(ventana, verde, (300, 30), (300, 560), 2)


fuente = pygame.font.Font(None, 30)
texto1 = fuente.render("X", 0, (rojo))
texto2 = fuente.render("Y", 0, (rojo))
texto3 = fuente.render("200 x 90 ", 0, (azul_marino))

palabra_sistema = pygame.font.SysFont("Century Gothic", 15)
texto_sistema = palabra_sistema.render("200", 0, (blanco))

fuente_sistema = pygame.font.SysFont("Century Gothic", 15)
letra_sistema = fuente_sistema.render("90", 0, (blanco))

fuent_sistema = pygame.font.SysFont("Century Gothic", 25)
sila_sistema = fuent_sistema.render("|", 0, (verde))

fuen_sistema = pygame.font.SysFont("Century Gothic", 25)
word_sistema = fuen_sistema.render("--", 0, (verde))

f_sistema = pygame.font.SysFont("Century Gothic", 25)
pala_sistema = f_sistema.render("|", 0, (verde))

f_sistema = pygame.font.SysFont("Century Gothic", 25)
pa_sistema = f_sistema.render("|", 0, (verde))

fue_sistema = pygame.font.SysFont("Century Gothic", 25)
wor_sistema = fue_sistema.render("--", 0, (verde))

fu_sistema = pygame.font.SysFont("Century Gothic", 25)
w_sistema = fu_sistema.render("--", 0, (verde))

a_sistema = pygame.font.SysFont("Century Gothic", 15)
b_sistema = a_sistema.render("2", 0, (rojo))

c_sistema = pygame.font.SysFont("Century Gothic", 15)
d_sistema = c_sistema.render("1", 0, (rojo))

e_sistema = pygame.font.SysFont("Century Gothic", 15)
g_sistema = e_sistema.render("1", 0, (rojo))

h_sistema = pygame.font.SysFont("Century Gothic", 15)
i_sistema = h_sistema.render("2", 0, (rojo))


class Punto():
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '({},{})'.format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje Y"
        else:
             return "sobre el origen"
        return self.x and self.y

    def vector(self, p):
        vector_resul = Punto(p.x - self.x, p.y - self.y)
        return vector_resul

    def distancia(self, p):
        distan = math.sqrt((p.x - self.x)**2 + (p.y - self.y)**2)
        return distan


class Rectangulo():

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def area(self):
        return self.base()*self.altura()


if __name__ == '__main__':

    A = Punto(2, 3)
    B = Punto(5, 5)
    C = Punto(-3, -1)
    D = Punto(0, 0)

    print(f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print(f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}")
    print(f"El punto {D} se encuentra {D.cuadrante()}")

    print(f"La distancia entre el punto {A} y {B} es: {A.distancia(B)}")
    print(f"La distancia entre el punto {B} y {A} es: {B.distancia(A)}")

    DA = A.distancia(D)
    DB = B.distancia(D)
    DC = C.distancia(D)
    if DA > DB and DA > DC:
        print(f"El punto {A} se encuentra más lejos del origen.")
    elif DB > DA and DB > DC:
        print(f"El punto {B} se encuentra más lejos del origen.")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rsta = Rectangulo(A, B)
    print("La base del rectángulo es: {}".format(rsta.base()))
    print("La altura del rectángulo es: {}".format(rsta.altura()))
    print("El área del rectángulo es: %.2f" % rsta.area())

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        ventana.blit(texto1, (570, 290))
        ventana.blit(texto2, (295, 570))
        ventana.blit(texto3, (355, 335))
        ventana.blit(b_sistema, (270, 328))
        ventana.blit(d_sistema, (270, 309))
        ventana.blit(g_sistema, (314, 260))
        ventana.blit(i_sistema, (331, 260))
        ventana.blit(texto_sistema, (485, 260))
        ventana.blit(letra_sistema, (260, 380))
        ventana.blit(sila_sistema, (492, 270))
        ventana.blit(word_sistema, (288, 370))
        ventana.blit(pala_sistema, (313, 270))
        ventana.blit(pa_sistema, (330, 270))
        ventana.blit(wor_sistema, (288, 318))
        ventana.blit(w_sistema, (288, 300))

        pygame.display.update()